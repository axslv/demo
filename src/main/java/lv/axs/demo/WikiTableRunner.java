package lv.axs.demo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lv.axs.demo.jpa.repositories.CurrencyRepository;
import lv.axs.demo.jpa.services.WikiTableService;
import lv.axs.demo.model.Currency;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
//@Controller ))
public class WikiTableRunner implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(WikiTableRunner.class);

    @Autowired
    private CurrencyRepository currencyRepository;
    
    @Autowired
    private WikiTableService wikiService;
    
    @Override
    public void run(String... args) throws Exception {
        List<Currency> lst = new ArrayList<>();
        lst = wikiService.fetch().getCurrencyList();
        lst.forEach((Currency cs) -> {            

                currencyRepository.save(cs);
                    
        });
        
        LOG.info("Currencies ready. Size of list: " + lst.size());
    }

}
