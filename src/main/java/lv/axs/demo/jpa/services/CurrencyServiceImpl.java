package lv.axs.demo.jpa.services;

import java.util.List;
import lv.axs.demo.jpa.repositories.CurrencyRepository;
import lv.axs.demo.model.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl() {

    }

    @Override
    public Currency findByCode(String name) {
        return currencyRepository.findByCurrencyAlfabetic(name);
    }

    @Override
    public void saveCurrency(Currency user) {
        currencyRepository.save(user);
    }

    public void updateCurrency(Currency user) {
        saveCurrency(user);
    }

    @Override
    public void deleteAllCurrencies() {
        currencyRepository.deleteAll();
    }

    @Override
    public List<Currency> findAllCurrencies() {
        return currencyRepository.findAll();
    }

    @Override
    public boolean isCurrencyExists(Currency user) {
        return findByCode(user.getCurrencyAlfabetic()) != null;
    }

    @Override
    public Currency findById(Long id) {
        return currencyRepository.getOne(id);
    }

}
