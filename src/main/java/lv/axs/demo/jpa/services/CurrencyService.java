package lv.axs.demo.jpa.services;

import java.util.List;
import lv.axs.demo.model.Currency;

public interface CurrencyService  {
    
    Currency findById(Long id);
    
    Currency findByCode(String currencyAlfabetic);
    
    void saveCurrency(Currency cr);
    
    void deleteAllCurrencies();
    
    List<Currency> findAllCurrencies();
    
    boolean isCurrencyExists(Currency cr);
    
}
