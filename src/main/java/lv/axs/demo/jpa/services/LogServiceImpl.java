package lv.axs.demo.jpa.services;

import java.util.List;
import lv.axs.demo.jpa.repositories.LogRepository;
import lv.axs.demo.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    public LogServiceImpl() {
        
    }

    @Override
    public void saveLogEntry(Log entry) {
        logRepository.save(entry);
    }

    @Override
    public void deleteAllLogEntries() {
        logRepository.deleteAll();
    }

    @Override
    public Page<Log> tail(Integer pageNumber, Integer pageSize) {
        PageRequest request = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.DESC, "startTime");
        return logRepository.findAll(request);
    }
    
    @Override
   public List<Log> findAll() {
        return logRepository.findAll();
    }

}
