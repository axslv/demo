package lv.axs.demo.jpa.services;

import java.io.IOException;
import lv.axs.demo.model.Currency;
import lv.axs.demo.model.WikiTable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WikiTableServiceImpl implements WikiTableService {

    private WikiTable wikiTable = new WikiTable();

    @Autowired
    public WikiTableServiceImpl() {

    }

    @Override
    public WikiTable fetch() throws IOException {
        Element elly = new Element(Tag.valueOf("<p>"), "");
        Document document = Jsoup.connect(wikiTable.getResourceAddress()).get();
        Elements elements = document.select("table.wikitable.sortable");

        for (Element el : elements) {
            if (el.html().contains("United Arab Emirates dirham")) {
                elly = el;
            }
        }
        Elements trs = elly.select("tr");
        Currency cs;

        for (int i = 0; i < trs.size(); i++) {
            cs = new Currency();
            Elements tds = trs.get(i).select("td");

            for (int j = 0; j < tds.size(); j++) {
                cs.setCurrencyAlfabetic(tds.get(0).text());
                cs.setCurrencyNumeric(tds.get(1).text());
                cs.setCurrencyFullName(tds.get(3).text());

                if (".".equals(tds.get(2).text().trim())) {
                    cs.setDecimalPlaces(-1);
                    continue;
                }
                cs.setDecimalPlaces(Integer.parseInt(tds.get(2).text()
                        .replaceAll("\\*", "")
                        .replaceAll("\\[\\d+\\]", "").trim())
                );
            }
            
            if (null != cs.getCurrencyAlfabetic()) {
                wikiTable.getCurrencyList().add(cs);
            }
            
        }        
        return this.wikiTable;

    }
}
