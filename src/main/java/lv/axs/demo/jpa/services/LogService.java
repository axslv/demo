package lv.axs.demo.jpa.services;

import java.util.List;
import lv.axs.demo.model.Log;
import org.springframework.data.domain.Page;

public interface LogService {

    void saveLogEntry(Log cr);
 
    void deleteAllLogEntries();
 
    Page<Log> tail(Integer pageNumber, Integer pageSize); 
    
    List<Log> findAll();
    
    
}
