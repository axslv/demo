package lv.axs.demo.jpa.services;

import java.io.IOException;
import lv.axs.demo.model.WikiTable;

public interface WikiTableService  {
    
    WikiTable fetch() throws IOException;
    
    
}
