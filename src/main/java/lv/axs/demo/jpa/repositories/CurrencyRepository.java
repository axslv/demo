package lv.axs.demo.jpa.repositories;

import lv.axs.demo.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {
    Currency findByCurrencyAlfabetic(String currencyAlfabetic);
}
