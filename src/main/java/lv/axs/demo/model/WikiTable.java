package lv.axs.demo.model;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;


public class WikiTable {
    
    private List<Currency> currencyList = new ArrayList<>();
    private Element element = new Element(Tag.valueOf("<p>"), "");
    private final String resourceAddress = "https://en.wikipedia.org/wiki/ISO_4217";
    
    public void fetchTable() {
        
    }
    
    public Element getElement() {
        return this.element;
    }

    /**
     * @return the resourceAddress
     */
    public String getResourceAddress() {
        return resourceAddress;
    }

    /**
     * @return the currencyList
     */
    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    /**
     * @param element the element to set
     */
    public void setElement(Element element) {
        this.element = element;
    }
    
    
    
    
    
    
}
