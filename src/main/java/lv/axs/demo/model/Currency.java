package lv.axs.demo.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "APP_CURENCY", indexes = {
    @Index(columnList = "CURRENCY_CODE", unique = true, name = "appc_cur_code_hidx")
})
public class Currency implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Size(min = 3, message = "Currency alfabetic code should have atleast 3 characters")
    @Column(name = "CURRENCY_CODE", nullable = false)
    private String currencyAlfabetic;

    @NotEmpty
    @Column(name = "CURRENCY_NAME", nullable = false)
    private String currencyFullName = "No name";

    @NotEmpty
    @Column(name = "CURRENCY_NUM", nullable = false)
    private String currencyNumeric = "000";

    @Column(name = "DECIMAL_PLACES", nullable = false)
    private Integer decimalPlaces = -1;

    public Currency() {
    }

    public Currency(String currencyAlfabetic) {
        this.currencyAlfabetic = currencyAlfabetic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the currencyAlfabetic
     */
    public String getCurrencyAlfabetic() {
        return currencyAlfabetic;
    }

    /**
     * @param currencyAlfabetic the currencyAlfabetic to set
     */
    public void setCurrencyAlfabetic(String currencyAlfabetic) {
        this.currencyAlfabetic = currencyAlfabetic.toLowerCase()
                .replaceAll("\\*", "")
                .replaceAll("\\[\\d+\\]", "").trim();
    }

    /**
     * @return the decimalPlaces
     */
    public Integer getDecimalPlaces() {
        return decimalPlaces;
    }

    /**
     * @param decimalPlaces the decimalPlaces to set
     */
    public void setDecimalPlaces(Integer decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    /**
     * @return the currencyFullName
     */
    public String getCurrencyFullName() {
        return currencyFullName;
    }

    /**
     * @param currencyFullName the currencyFullName to set
     */
    public void setCurrencyFullName(String currencyFullName) {
        this.currencyFullName = currencyFullName
                .replaceAll("\\*", "")
                .replaceAll("\\[\\d+\\]", "").trim();
    }

    /**
     * @return the currencyNumeric
     */
    public String getCurrencyNumeric() {
        return currencyNumeric;
    }

    /**
     * @param currencyNumeric the currencyNumeric to set
     */
    public void setCurrencyNumeric(String currencyNumeric) {
        this.currencyNumeric = currencyNumeric;
    }

}
