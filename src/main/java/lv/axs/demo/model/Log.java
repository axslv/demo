package lv.axs.demo.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "APP_LOGS")
public class Log implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Lob
    @Column(name = "ENTRY_TEXT", nullable = false, length=2048)
    private String entryText;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TS", nullable = false)
    private Date creationDate = new Date(System.currentTimeMillis());
    
    public Log() {
        
    }
    
    public Log(String entryText) {
        this.entryText = entryText;
    }

      public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the entryText
     */
    public String getEntryText() {
        return entryText;
    }

    /**
     * @param entryText the entryText to set
     */
    public void setEntryText(String entryText) {
        this.entryText = entryText;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


}
