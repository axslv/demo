package lv.axs.demo;

import java.util.List;
import lv.axs.demo.jpa.services.LogService;
import lv.axs.demo.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
//@RequestMapping("/by-name")
public class LogServiceController {

    //  @ExceptionHandler(Exception.class)
    //  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    @Autowired
    private LogService loggingService;

    private static final int PAGE_SIZE = 50;

    @RequestMapping(value = "/log/pages/{pageNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Log> getRunbookPage(@PathVariable Integer pageNumber) {
        Page<Log> page = loggingService.tail(pageNumber, PAGE_SIZE);
        int current = page.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, page.getTotalPages());
        /* 
        model.addAttribute("logEntries", page);
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);
         */
        return page.getContent();
    }

    @RequestMapping(value = "/log/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Log> getLatestLog() {
        Page<Log> page = loggingService.tail(0, PAGE_SIZE);
        int current = page.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, page.getTotalPages());
        return page.getContent();
    }

    public void handleError() {

    }

}
