package lv.axs.demo;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lv.axs.demo.jpa.services.CurrencyService;
import lv.axs.demo.jpa.services.LogService;
import lv.axs.demo.model.Currency;
import lv.axs.demo.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@RestController
@RequestMapping("/currency")
public class CurrencyServiceController {

    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private LogService logService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Currency getCurrencyByCode(@Valid @RequestBody Currency currency) {
        
        Currency found = currencyService.findByCode(currency.getCurrencyAlfabetic());
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        
        String ip = request.getRemoteAddr();
        StringBuilder sb = new StringBuilder();
        sb.append("Client with IP address ").append(ip).append(" requested currency with code=").append(currency.getCurrencyAlfabetic());
        Log log = new Log(sb.toString());        
        logService.saveLogEntry(log);
        return found;
    }

}
