package lv.axs.demo.exc;


import javax.servlet.http.HttpServletRequest;
import lv.axs.demo.jpa.services.LogService;
import lv.axs.demo.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

    @Autowired
    private LogService logService;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleError(HttpServletRequest request, Exception e) {
        StringBuilder sb = new StringBuilder()
                .append("An exception was raised from ")
                .append(request.getRemoteAddr())
                .append("<br />")
                .append(e.getMessage());
        Log lg = new Log(sb.toString());
        logService.saveLogEntry(lg);
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e));
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
    
//@TODO correctly handle NOT FOUND
//@TODO implement custom error messages
//@TODO implement more detailed exception handling (e.g. malformed request or validation problems)
//@TODO return particular descriptive error messages to the consumer
    //all of this is out of assignment scope for now
}
