package lv.axs.demo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lv.axs.demo.jpa.services.WikiTableService;
import lv.axs.demo.jpa.services.WikiTableServiceImpl;
import lv.axs.demo.model.Currency;
import org.assertj.core.api.AbstractBooleanAssert;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class WikiTableServiceTest {

    @TestConfiguration
    static class LogServiceImplTestContextConfiguration {

        @Bean
        public WikiTableService wikiTableService() {
            return new WikiTableServiceImpl();
        }
    }
    @Autowired
    private WikiTableService wikiService;

    @Before
    public void setUp() {        
        
    }

    @Test
    public void whenFetchSuccess_thenVerticalView() {

        List<Currency> found = new ArrayList<>();
        try {
            found = wikiService.fetch().getCurrencyList();
        } catch (IOException ex) {
            Logger.getLogger(WikiTableServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertThat(found.size())
                .isEqualTo(178);        
    } 
    
    @Test    
    public void whenFetchSuccess_thenHorizontalView() {

        List<Currency> found = new ArrayList<>();
        Currency toTest = new Currency();
        
        try {
            found = wikiService.fetch().getCurrencyList();
        } catch (IOException ex) {
            Logger.getLogger(WikiTableServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertThat(null != found.get(0).getCurrencyAlfabetic()); 
        assertThat(!toTest.getCurrencyFullName().equals(found.get(0).getCurrencyFullName()));
        assertThat(!toTest.getCurrencyNumeric().equals(found.get(0).getCurrencyNumeric()));
        assertThat(!toTest.getDecimalPlaces().equals(found.get(0).getDecimalPlaces()));       
        
        
    }   
    
    
}
