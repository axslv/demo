package lv.axs.demo.service;

import java.util.ArrayList;
import java.util.List;
import lv.axs.demo.jpa.repositories.LogRepository;
import lv.axs.demo.jpa.services.LogService;
import lv.axs.demo.jpa.services.LogServiceImpl;
import lv.axs.demo.model.Log;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LogServiceImplTest {

    @TestConfiguration
    static class LogServiceImplTestContextConfiguration {

        @Bean
        public LogService logService() {
            return new LogServiceImpl();
        }
    }
    @Autowired
    private LogService logService;

    @MockBean
    private LogRepository logRepository;

    @Before
    public void setUp() {
        Log lg = new Log("Message 1");
        Log lg1 = new Log("Message 2");
        Log lg2 = new Log("Message 3");
        List<Log> logList = new ArrayList<>();
        logList.add(lg);
        logList.add(lg1);
        logList.add(lg2);
        
        Mockito.when(logRepository.findAll())
                .thenReturn(logList);
    }

    @Test
    public void whenValidName_thenCurrencyShouldBeFound() {

        List<Log> found = logService.findAll();
        assertThat(found.size())
                .isEqualTo(3);
        
    }

}
