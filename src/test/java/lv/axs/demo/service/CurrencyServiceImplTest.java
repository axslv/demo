package lv.axs.demo.service;

import lv.axs.demo.jpa.repositories.CurrencyRepository;
import lv.axs.demo.jpa.services.CurrencyService;
import lv.axs.demo.jpa.services.CurrencyServiceImpl;
import lv.axs.demo.model.Currency;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CurrencyServiceImplTest {

    @TestConfiguration
    static class CurrencyServiceImplTestContextConfiguration {

        @Bean
        public CurrencyService currencyService() {
            return new CurrencyServiceImpl();
        }
    }
    @Autowired
    private CurrencyService currencyService;

    @MockBean
    private CurrencyRepository currencyRepository;

    @Before
    public void setUp() {
        Currency cr = new Currency("usd");
        Mockito.when(currencyRepository.findByCurrencyAlfabetic(cr.getCurrencyAlfabetic()))
                .thenReturn(cr);
    }

    @Test
    public void whenValidName_thenCurrencyShouldBeFound() {
        String name = "usd";
        Currency found = currencyService.findByCode(name);
        assertThat(found.getCurrencyAlfabetic())
                .isEqualTo(name);
    }
}
