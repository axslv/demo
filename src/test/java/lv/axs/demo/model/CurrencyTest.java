package lv.axs.demo.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CurrencyTest {

    public CurrencyTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setCurrencyAlfabetic method, of class Currency.
     */
    @Test
    public void testSetGetCurrencyAlfabetic() {
        String currencyAlfabetic = "uSd";
        Currency instance = new Currency();
        instance.setCurrencyAlfabetic(currencyAlfabetic);
        assertTrue(currencyAlfabetic.toLowerCase().equals(instance.getCurrencyAlfabetic()));
    }

    /**
     * Test of setCurrencyAlfabetic method with expected data, of class
     * Currency.
     */
    @Test
    public void testSetGetCurrencyAlfabeticDistracted() {
        String currencyAlfabetic = "uSd [2]";
        String currencyAlfabeticRight = "usd";
        Currency instance = new Currency();
        instance.setCurrencyAlfabetic(currencyAlfabetic);
        assertTrue(currencyAlfabeticRight.equals(instance.getCurrencyAlfabetic()));
    }

    @Test
    public void testSetGetCurrencyAlfabeticDistractedMore() {
        String currencyAlfabetic = "uSd* [2]";
        String currencyAlfabeticRight = "usd";
        Currency instance = new Currency();
        instance.setCurrencyAlfabetic(currencyAlfabetic);
        assertTrue(currencyAlfabeticRight.equals(instance.getCurrencyAlfabetic()));
    }

    @Test
    public void testSetGetCurrencyAlfabeticDistractedMoreAndMore() {
        String currencyAlfabetic = "uSd* [2]";
        String currencyAlfabeticRight = "usd";
        Currency instance = new Currency();
        instance.setCurrencyAlfabetic(currencyAlfabetic);
        assertTrue(currencyAlfabeticRight.equals(instance.getCurrencyAlfabetic()));
    }

    /**
     * Test of setCurrencyFullName method with expected data, of class Currency.
     */
    @Test
    public void testSetGetCurrencyFullName() {
        String currencyFullName = "US Dollar";
        Currency instance = new Currency();
        instance.setCurrencyFullName(currencyFullName);
        assertTrue(currencyFullName.equals(instance.getCurrencyFullName()));
    }

    @Test
    public void testSetGetCurrencyFullNameDistracted() {
        String currencyFullName = "US Dollar [11]";
        String currencyFullNameCorrect = "US Dollar";
        Currency instance = new Currency();
        instance.setCurrencyFullName(currencyFullName);
        assertTrue(currencyFullNameCorrect.equals(instance.getCurrencyFullName()));
    }

    @Test
    public void testSetGetCurrencyFullNameDistractedMore() {
        String currencyFullName = "US Dollar* [11]";
        String currencyFullNameCorrect = "US Dollar";
        Currency instance = new Currency();
        instance.setCurrencyFullName(currencyFullName);
        assertTrue(currencyFullNameCorrect.equals(instance.getCurrencyFullName()));
    }

    @Test
    public void testSetGetCurrencyFullNameDistractedMoreAndMore() {
        String currencyFullName = "US Dollar* [11]*";
        String currencyFullNameCorrect = "US Dollar";
        Currency instance = new Currency();
        instance.setCurrencyFullName(currencyFullName);
        assertTrue(currencyFullNameCorrect.equals(instance.getCurrencyFullName()));
    }

}
