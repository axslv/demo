package lv.axs.demo.repo;


import lv.axs.demo.jpa.repositories.CurrencyRepository;
import lv.axs.demo.model.Currency;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class CurrencyRepositoryTest {


    @Autowired
    private CurrencyRepository currencyRepository;

    @Test
    public void whenFindByName_thenReturnCurrency() {
        Currency cs = new Currency("uss") ;
        cs.setCurrencyFullName("US Dollar");
        cs.setCurrencyNumeric("000");
        cs.setDecimalPlaces(2);
        currencyRepository.save(cs);

        Currency found = currencyRepository.findByCurrencyAlfabetic(cs.getCurrencyAlfabetic());

        assertThat(found.getCurrencyAlfabetic())
                .isEqualTo(cs.getCurrencyAlfabetic());
    }

}
