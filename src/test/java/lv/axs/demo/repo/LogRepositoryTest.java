package lv.axs.demo.repo;


import lv.axs.demo.jpa.repositories.LogRepository;
import lv.axs.demo.model.Currency;
import lv.axs.demo.model.Log;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class LogRepositoryTest {


    @Autowired
    private LogRepository logRepository;

    @Test
    public void whenFindByName_thenReturnLog() {
        // given
        Log lg = new Log("Test error message") ;
        
        logRepository.save(lg);
        
        /*
            ID is always 1 as we assume log table is empty at our temp database? 
            We could also create findRecordByText method, but keeping in mind that id=1 
            is still sufficient for our cause
        */
        Log found = logRepository.findById(1L).get(); 

        assertThat(found.getEntryText())
                .isEqualTo(lg.getEntryText());
    }

}
